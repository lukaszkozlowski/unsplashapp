import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search_image_app/api/exceptions.dart';
import 'package:search_image_app/api/image_service.dart';
import 'package:search_image_app/bloc/images/image_events.dart';
import 'package:search_image_app/model/image_model.dart';
import 'package:search_image_app/model/searched_image_model.dart';
import '../images/image_states.dart';

class ImagesBloc extends Bloc<ImageEvent, ImageState> {
  List<ImageModel> imageList;
  SearchedImageModel searchedImageList;
  final ImagesRepo imagesRepo;

  ImagesBloc({this.imagesRepo}) : super(ImagesInitState());

  @override
  Stream<ImageState> mapEventToState(ImageEvent event) async* {
    if (event is GetRandomImageEvent) {
      yield ImagesLoading();
      try {
        imageList = await imagesRepo.getRandomImages();
        yield ImagesLoaded(images: imageList);
      } on SocketException {
        yield ImagesListError(error: NoInternetException('No internet'));
      } on HttpException {
        yield ImagesListError(
            error: NoServiceFoundException('No service found'));
      } on FormatException {
        yield ImagesListError(
            error: InvalidFormatException('Invalid response format'));
      } catch (e) {
        yield ImagesListError(error: UnknownException('Unknown error'));
      }
    } else if (event is SearchedImageEvent) {
      yield ImagesLoading();
      try {
        searchedImageList = await imagesRepo.searchImage(event.query);
        yield SearchedImageLoaded(searchedImageModel: searchedImageList);
      } on SocketException {
        yield ImagesListError(error: NoInternetException('No internet'));
      } on HttpException {
        yield ImagesListError(
            error: NoServiceFoundException('No service found'));
      } on FormatException {
        yield ImagesListError(
            error: InvalidFormatException('Invalid response format'));
      } catch (e) {
        print(e.toString());
        yield ImagesListError(error: UnknownException('Unknown error'));
      }
    }
  }
}
