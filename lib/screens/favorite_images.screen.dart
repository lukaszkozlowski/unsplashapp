import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_bloc.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_event.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_state.dart';
import 'package:search_image_app/widget/loading.dart';

class FavoriteImagesScreen extends StatefulWidget {
  @override
  _FavoriteImagesScreenState createState() => _FavoriteImagesScreenState();
}

class _FavoriteImagesScreenState extends State<FavoriteImagesScreen> {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<FavoriteImageBloc>(context);
    return BlocBuilder<FavoriteImageBloc, FavoritesState>(
        builder: (context, state) {
      if (state is FavoritesLoading)
        return Loading();
      else if (state is FavoritesLoaded) {
        return ListView.builder(
            itemCount: state.images.length,
            itemBuilder: (context, index) {
              final image = state.images[index];
              return ListTile(
                title: Text(image.altDescription ?? ""),
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(image.urlThumb),
                ),
                trailing: Container(
                  width: 100,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () {
                            setState(() {
                              bloc.add(RemoveFromFavorites(image));
                            });
                          }),
                    ],
                  ),
                ),
              );
            });
      } else if (state is FavoritesError)
        return Text(state.message ?? "");
      else
        return Container();
    });
  }
}
