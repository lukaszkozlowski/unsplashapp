import 'package:equatable/equatable.dart';
import 'package:search_image_app/model/image_model.dart';
import 'package:search_image_app/model/searched_image_model.dart';

abstract class ImageState extends Equatable {
  @override
  List<Object> get props => [];
}

class ImagesInitState extends ImageState {}

class ImagesLoading extends ImageState {}

// ignore: must_be_immutable
class ImagesLoaded extends ImageState {
  @override
  List<Object> get props => [images];
  List<ImageModel> images;

  ImagesLoaded({this.images});
}

// ignore: must_be_immutable
class SearchedImageLoaded extends ImageState {
  @override
  List<Object> get props => [searchedImageModel];
  SearchedImageModel searchedImageModel;

  SearchedImageLoaded({this.searchedImageModel});
}

class ImagesListError extends ImageState {
  final error;

  ImagesListError({this.error});
}
