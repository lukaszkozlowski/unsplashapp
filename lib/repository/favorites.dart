import 'package:hive/hive.dart';
import 'package:search_image_app/model/image_model.dart';

abstract class FavoriteRepository {
  Future<void> add(ImageModel model);

  Future<void> delete(ImageModel model);

  Future<List<ImageModel>> favorites();
}

class LocalFavoritesRepository implements FavoriteRepository {
  Box _box;

  LocalFavoritesRepository() {
    _box = Hive.box("favorites");
  }

  @override
  Future<void> add(ImageModel model) async {
    return _box.put(model.id, model.toJson());
  }

  @override
  Future<void> delete(ImageModel model) {
    return _box.delete(model.id);
  }

  @override
  Future<List<ImageModel>> favorites() {
    return Future.value(_box.values
        .map((e) => ImageModel.fromJson(Map<String, dynamic>.from(e)))
        .toList());
  }
}
