import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_event.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_state.dart';
import 'package:search_image_app/model/favorite_image_model.dart';
import 'package:search_image_app/model/image_model.dart';
import 'package:search_image_app/repository/favorites.dart';

class FavoriteImageBloc extends Bloc<FavoriteImageEvent, FavoritesState> {
  FavoriteImageBloc({
    @required this.repository,
  }) : super(FavoritesLoading());

  final LocalFavoritesRepository repository;

  @override
  Stream<FavoritesState> mapEventToState(FavoriteImageEvent event) async* {
    if (event is AddToFavorites) {
      repository.add(event.imageModel);
      if (state is FavoritesLoaded) {
        yield FavoritesLoaded(
            (state as FavoritesLoaded).images..add(event.imageModel));
      }
    } else if (event is RemoveFromFavorites) {
      repository.delete(event.imageModel);
      if (state is FavoritesLoaded) {
        yield FavoritesLoaded(
            (state as FavoritesLoaded).images..remove(event.imageModel));
      }
    } else if (event is InitFavorites) {
      yield FavoritesLoading();
      List<ImageModel> items = await repository.favorites();
      yield FavoritesLoaded(items);
    }
  }
}
