import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_bloc.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_event.dart';
import 'package:search_image_app/model/image_model.dart';
import 'package:intl/intl.dart';
import 'package:search_image_app/model/searched_image_model.dart';

// ignore: must_be_immutable
class ImageDetailScreen extends StatefulWidget {
  final ImageModel image;

  ImageDetailScreen({this.image});

  @override
  _ImageDetailScreenState createState() => _ImageDetailScreenState();
}

class _ImageDetailScreenState extends State<ImageDetailScreen> {
  _getDescription() {
    if (widget.image.description != null &&
        widget.image.altDescription != null) {
      return widget.image.description + ' ' + widget.image.altDescription;
    } else if (widget.image.description != null &&
        widget.image.altDescription == null) {
      return widget.image.description;
    } else if (widget.image.description == null &&
        widget.image.altDescription != null) {
      return widget.image.altDescription;
    } else {
      return 'No description';
    }
  }

  void addRandomImageToFavorite(ImageModel image) async {
    final imageBox = await Hive.openBox('favoriteImagesBox');
    imageBox.add(image);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: (widget.image.height) / 10,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              background: Hero(
                tag: widget.image.id,
                child: Image.network(
                  widget.image.urlRegular,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          SliverList(
              delegate: SliverChildListDelegate([
            Row(children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: ClipRRect(
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/user.jpg',
                    image: widget.image.userImage,
                    width: 70,
                    height: 70,
                  ),
                  borderRadius: BorderRadius.circular(25),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Author: ' + widget.image.userName,
                      style: TextStyle(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                        'Created date: ${DateFormat.yMMMd().format(widget.image.createDate)}',
                        textAlign: TextAlign.left)
                  ],
                ),
              )
            ]),

            SizedBox(
              height: 30,
            ),
            Text(
              'DESCRIPTION',
              style: TextStyle(fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 100),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(10)),
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.all(10),
                        height: 100,
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: Text(_getDescription())),
                  ],
                ),
              ),
            )
            // Container(
          ]))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.orange,
        child: Icon(
          Icons.favorite,
        ),
        onPressed: () {
          BlocProvider.of<FavoriteImageBloc>(context)
              .add(AddToFavorites(widget.image));
        },
      ),
    );
  }
}

// ignore: must_be_immutable
class SearchedImageDetailScreen extends StatefulWidget {
  final Result result;

  SearchedImageDetailScreen({this.result});

  @override
  _SearchedImageDetailScreenState createState() =>
      _SearchedImageDetailScreenState();
}

class _SearchedImageDetailScreenState extends State<SearchedImageDetailScreen> {
  addSearchedImageToFavorite(Result result) async {
    final imageBox = await Hive.openBox<Result>('favoriteImagesBox');
    imageBox.add(result);
  }

  _getDescription() {
    if (widget.result.description != null &&
        widget.result.altDescription != null) {
      return widget.result.description + ' ' + widget.result.altDescription;
    } else if (widget.result.description != null &&
        widget.result.altDescription == null) {
      return widget.result.description;
    } else if (widget.result.description == null &&
        widget.result.altDescription != null) {
      return widget.result.altDescription;
    } else {
      return 'No description';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              expandedHeight: (widget.result.height) / 10,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                background: Hero(
                  tag: widget.result.id,
                  child: Image.network(
                    widget.result.urlRegular,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SliverList(
                delegate: SliverChildListDelegate([
              Row(children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10),
                  child: ClipRRect(
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/user.jpg',
                      image: widget.result.userImage,
                      width: 70,
                      height: 70,
                    ),
                    borderRadius: BorderRadius.circular(25),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Author: ' + widget.result.userName,
                        style: TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.left,
                      ),
                      Text(
                          'Created date: ${DateFormat.yMMMd().format(widget.result.createDate)}',
                          textAlign: TextAlign.left)
                    ],
                  ),
                )
              ]),

              SizedBox(
                height: 30,
              ),
              Text(
                'DESCRIPTION',
                style: TextStyle(fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 100),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10)),
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.all(10),
                          height: 100,
                          width: MediaQuery.of(context).size.width * 0.7,
                          child: Text(_getDescription())),
                    ],
                  ),
                ),
              )
              // Container(
            ]))
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.orange,
          child: Icon(Icons.favorite),
          onPressed: () {
            BlocProvider.of<FavoriteImageBloc>(context)
                .add(AddToFavorites(widget.result.toImageModel()));
          },
        ));
  }
}
