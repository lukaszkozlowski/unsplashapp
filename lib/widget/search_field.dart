import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  _query(String query) {
    return query;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            child: Column(children: <Widget>[
      Flexible(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            decoration: InputDecoration(
                hintText: 'Enter a word', suffixIcon: Icon(Icons.search)),
            onChanged: (value) {
              _query(value);
            },
          ),
        ),
      ),
    ])));
  }
}
