import 'package:flutter/material.dart';
import 'package:search_image_app/model/image_model.dart';
import 'package:search_image_app/model/searched_image_model.dart';
import '../screens/image_detail_screen.dart';

class ImageItem extends StatelessWidget {
  final ImageModel imageModel;

  ImageItem({this.imageModel});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(PageRouteBuilder(
              pageBuilder: (context, animation, _) {
                return FadeTransition(
                  opacity: animation,
                  child: ImageDetailScreen(
                    image: imageModel,
                  ),
                );
              },
            ));
          },
          child: Hero(
            tag: imageModel.id,
            child: FadeInImage(
              placeholder: NetworkImage(imageModel.urlThumb),
              image: NetworkImage(imageModel.urlThumb),
              fit: BoxFit.cover,
            ),
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          title: _description(),
        ),
      ),
    );
  }

  _description() {
    if (imageModel.title == null || imageModel.title == '') {
      if (imageModel.description != null && imageModel.altDescription != null) {
        return Text(
          imageModel.description,
          textAlign: TextAlign.start,
        );
      } else if (imageModel.description == null &&
          imageModel.altDescription != null) {
        return Text(
          imageModel.altDescription,
          textAlign: TextAlign.start,
        );
      } else if (imageModel.description == null &&
              imageModel.altDescription == null ||
          imageModel.description == null && imageModel.altDescription == '' ||
          imageModel.description == '' && imageModel.altDescription == null ||
          imageModel.description == '' && imageModel.altDescription == '') {
        return Text('No title and description', textAlign: TextAlign.start);
      }
    } else {
      return Text(imageModel.title);
    }
  }
}

// ignore: must_be_immutable
class SearchedImageItem extends StatelessWidget {
  final Result result;

  SearchedImageItem({this.result});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(PageRouteBuilder(
              pageBuilder: (context, animation, _) {
                return FadeTransition(
                  opacity: animation,
                  child: SearchedImageDetailScreen(
                    result: result,
                  ),
                );
              },
            ));
          },
          child: Hero(
            tag: result.id,
            child: FadeInImage(
              placeholder: NetworkImage(result.urlThumb),
              image: NetworkImage(result.urlThumb),
              fit: BoxFit.cover,
            ),
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          title: _description(),
        ),
      ),
    );
  }

  _description() {
    if (result.title == null || result.title == '') {
      if (result.description != null && result.altDescription != null) {
        return Text(
          result.description,
          textAlign: TextAlign.start,
        );
      } else if (result.description == null && result.altDescription != null) {
        return Text(
          result.altDescription,
          textAlign: TextAlign.start,
        );
      } else if (result.description == null && result.altDescription == null ||
          result.description == null && result.altDescription == '' ||
          result.description == '' && result.altDescription == null ||
          result.description == '' && result.altDescription == '') {
        return Text('No title and description', textAlign: TextAlign.start);
      }
    } else {
      return Text(result.title);
    }
  }
}
