import 'package:hive/hive.dart';

part 'favorite_image_model.g.dart';

@HiveType(typeId: 2)
class FavoriteImageModel {
  @HiveField(0)
  String id;
  @HiveField(1)
  String title;
  @HiveField(2)
  double width;
  @HiveField(3)
  double height;
  @HiveField(4)
  String description;
  @HiveField(5)
  String altDescription;
  @HiveField(6)
  String urlThumb;
  @HiveField(7)
  String urlRegular;
  @HiveField(8)
  String userImage;
  @HiveField(9)
  String userName;
  @HiveField(10)
  DateTime createDate;

  FavoriteImageModel(
      {this.id,
      this.title,
      this.width,
      this.height,
      this.description,
      this.altDescription,
      this.urlThumb,
      this.urlRegular,
      this.userImage,
      this.userName,
      this.createDate});
}
