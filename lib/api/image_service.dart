import 'package:http/http.dart' as http;
import 'package:search_image_app/model/searched_image_model.dart';
import '../model/image_model.dart';

abstract class ImagesRepo {
  Future<List<ImageModel>> getRandomImages();

  Future<SearchedImageModel> searchImage(String query);
}

class ImagesService implements ImagesRepo {
  static const _accessKey = 'J8NhFss2fLA2iCQjLcrQ2qk-JGPXQHRsDG0sKgc82AI';
  static const _basicUrl = 'api.unsplash.com';
  static const _getRandomImages = '/photos/random';
  static const _searchImages = '/search/collections';

  @override
  Future<List<ImageModel>> getRandomImages() async {
    Uri uri = Uri.https(
        _basicUrl, _getRandomImages, {'client_id': _accessKey, 'count': '30'});

    http.Response response = await http.get(uri);
    List<ImageModel> images = ImageModel().imagesFromJson(response.body);
    return images;
  }

  @override
  Future<SearchedImageModel> searchImage(String query) async {
    Uri uri = Uri.https(_basicUrl, _searchImages, {
      'client_id': _accessKey,
      'query': query,
    });
    http.Response response = await http.get(uri);
    print(response.statusCode);
    SearchedImageModel images = searchedImageModelFromJson(response.body);
    return images;
  }
}
