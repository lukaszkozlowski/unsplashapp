import 'dart:convert';
import 'package:hive/hive.dart';

part 'image_model.g.dart';

@HiveType(typeId: 1)
class ImageModel {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final String title;
  @HiveField(2)
  final double width;
  @HiveField(3)
  final double height;
  @HiveField(4)
  final String description;
  @HiveField(5)
  final String altDescription;
  @HiveField(6)
  final String urlThumb;
  @HiveField(7)
  final String urlRegular;
  @HiveField(8)
  final String userImage;
  @HiveField(9)
  final String userName;
  @HiveField(10)
  final DateTime createDate;


  ImageModel({this.id,
    this.title,
    this.width,
    this.height,
    this.description,
    this.altDescription,
    this.urlThumb,
    this.urlRegular,
    this.userImage,
    this.userName,
    this.createDate,
  });

  List<ImageModel> imagesFromJson(String str) {
    final data = json.decode(str);

    return List<ImageModel>.from(data.map((x) => ImageModel.fromJson(x)));
  }

  String imagesToJson(List<ImageModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

  factory ImageModel.fromJson(Map<String, dynamic> json) => ImageModel(
    id: json['id'],
    title: json['title'],
    width: double.parse(json['width']?.toString() ?? '0'),
    height: double.parse(json['height']?.toString() ?? '0'),
    description: json['description'],
    altDescription: json['alt_description'],
    urlRegular: json['urls']['regular'],
    urlThumb: json['urls']['thumb'],
    userImage: json['user']['profile_image']['large'],
    userName: json['user']['name'],
    createDate: DateTime.parse(json['created_at']?.toString()),
  );

  Map<String, dynamic> toJson() => {
    'id': this.id,
    'title': this.id,
    'width': this.width.toString(),
    'height': this.height.toString(),
    'description': this.description,
    'alt_description': this.altDescription,
    'urls': {'regular': this.urlRegular, 'thumb': this.urlThumb},
    'user': {
      'name': this.userName,
      'profile_image': {'large': this.userImage}
    },
    'created_at': this.createDate?.toIso8601String(),
  };

}
