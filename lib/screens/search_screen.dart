import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search_image_app/model/searched_image_model.dart';
import 'package:search_image_app/widget/error.dart';
import 'package:search_image_app/widget/images_grid.dart';
import 'package:search_image_app/widget/loading.dart';
import 'package:search_image_app/model/image_model.dart';
import '../bloc/images/image_states.dart';
import '../bloc/images/image_events.dart';
import '../bloc/images/image_bloc.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {

  @override
  void initState() {
    super.initState();
    _loadImages();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  Widget _body() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 5),
          child: TextField(
            decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[600]),
                    borderRadius: BorderRadius.all(Radius.circular(30))),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                    borderRadius: BorderRadius.all(Radius.circular(30))),
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.orange,
                ),
                hintText: 'Search',
                filled: true,
                fillColor: Colors.grey[100]),
            onChanged: (value) {
              setState(() {
                if (value == '') {
                  _loadImages();
                } else {
                  _loadSearchedImages(value);
                }
              });
            },
          ),
        ),
        BlocBuilder<ImagesBloc, ImageState>(
            builder: (BuildContext context, ImageState state) {
          if (state is ImagesListError) {
            final error = state.error;
            String message = '${error.message}\nTap to retry.';
            return ErrorTxt(
              message: message,
              onTap: () => _loadImages(),
            );
          }
          if (state is ImagesLoaded) {
            List<ImageModel> images = state.images;
            return _list(images);
          } else if (state is SearchedImageLoaded) {
            SearchedImageModel image = state.searchedImageModel;
            return _searchedImagesList(image);
          }
          return Loading();
        }),
      ],
    );
  }

  void _loadImages() {
    context.read<ImagesBloc>().add(GetRandomImageEvent());
  }

  void _loadSearchedImages(String query) {
    context.read<ImagesBloc>().add(SearchedImageEvent(query: query));
  }

  Widget _list(List<ImageModel> images) {
    return Expanded(child: ImagesGrid(imagesData: images));
  }

  Widget _searchedImagesList(SearchedImageModel results) {
    return Expanded(
        child: SearchedImageGrid(
      searchedImageModel: results,
    ));
  }
}
