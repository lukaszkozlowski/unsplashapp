import 'package:equatable/equatable.dart';
import 'package:search_image_app/model/image_model.dart';

abstract class FavoritesState extends Equatable {
  const FavoritesState();

  @override
  List<Object> get props => [];
}

class FavoritesLoading extends FavoritesState {}

class FavoritesLoaded extends FavoritesState {
  final List<ImageModel> images;

  FavoritesLoaded(this.images);

  @override
  List<Object> get props => []..addAll(images);
}

class FavoritesError extends FavoritesState {
  final String message;

  FavoritesError(this.message);

  @override
  List<Object> get props => [message];
}
