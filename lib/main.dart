import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:search_image_app/api/image_service.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_bloc.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_event.dart';
import 'package:search_image_app/bloc/images/image_bloc.dart';
import 'package:search_image_app/model/image_model.dart';
import 'package:search_image_app/repository/favorites.dart';
import 'package:search_image_app/screens/favorite_images.screen.dart';
import 'package:search_image_app/widget/loading.dart';
import './model/favorite_image_model.dart';
import 'screens/search_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Directory appDocumentDirectory =
      await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(ImageModelAdapter());
  Hive.registerAdapter(FavoriteImageModelAdapter());
  await Hive.openBox("favorites");
  runApp(MyApp());
}

// ignore: must_be_immutable
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    Hive.box('favoriteImagesBox').compact();
    Hive.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => LocalFavoritesRepository(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) => ImagesBloc(imagesRepo: ImagesService())),
          BlocProvider(
              create: (context) => FavoriteImageBloc(
                  repository: context.read<LocalFavoritesRepository>())
                ..add(InitFavorites())),
        ],
        child: MaterialApp(
          home: FutureBuilder(
            future: Hive.openBox('favoriteImagesBox'),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                } else {
                  return MyBottomNavigationBar();
                }
              }
              return Loading();
            },
          ),
        ),
      ),
    );
  }
}

class MyBottomNavigationBar extends StatefulWidget {
  @override
  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  int _currentIndex = 0;
  List<Widget> _children;

  @override
  void initState() {
    _children = [SearchScreen(), FavoriteImagesScreen()];
    super.initState();
  }

  void onTappedBar(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      appBar: AppBar(
        flexibleSpace: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/unsplash.jpg'),
                    fit: BoxFit.fitWidth))),
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: (onTappedBar),
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favorite',
          )
        ],
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.amber,
      ),
    );
  }
}
