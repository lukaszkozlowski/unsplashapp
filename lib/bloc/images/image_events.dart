import 'package:equatable/equatable.dart';

abstract class ImageEvent extends Equatable {}

class GetRandomImageEvent extends ImageEvent {
  @override
  List<Object> get props => [];
}

class SearchedImageEvent extends ImageEvent {
  final String query;

  SearchedImageEvent({this.query});

  @override
  List<Object> get props => [query];
}
