import 'package:flutter/material.dart';
import 'package:search_image_app/model/image_model.dart';
import 'package:search_image_app/model/searched_image_model.dart';
import 'package:search_image_app/widget/image_item.dart';

class ImagesGrid extends StatelessWidget {
  final List<ImageModel> imagesData;

  ImagesGrid({this.imagesData});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: imagesData.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 2 / 3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5),
      itemBuilder: (context, index) {
        return Container(
          child: ImageItem(
            imageModel: imagesData[index],
          ),
        );
      },
    );
  }
}

// ignore: must_be_immutable
class SearchedImageGrid extends StatelessWidget {
  SearchedImageModel searchedImageModel;

  SearchedImageGrid({this.searchedImageModel});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: searchedImageModel.results.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 2 / 3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5),
      itemBuilder: (context, index) {
        return Container(
          child: SearchedImageItem(
            result: searchedImageModel.results[index],
          ),
        );
      },
    );
  }
}
