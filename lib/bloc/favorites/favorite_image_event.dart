import 'package:equatable/equatable.dart';
import 'package:search_image_app/model/image_model.dart';

abstract class FavoriteImageEvent extends Equatable {
  const FavoriteImageEvent();

  @override
  List<Object> get props => [];
}

class InitFavorites extends FavoriteImageEvent {}

class AddToFavorites extends FavoriteImageEvent {
  final ImageModel imageModel;

  const AddToFavorites(this.imageModel);

  @override
  List<Object> get props => [imageModel];
}

class RemoveFromFavorites extends FavoriteImageEvent {
  final ImageModel imageModel;

  const RemoveFromFavorites(this.imageModel);

  @override
  List<Object> get props => [imageModel];
}
