import 'dart:convert';

import 'package:search_image_app/model/image_model.dart';

SearchedImageModel searchedImageModelFromJson(String str) =>
    SearchedImageModel.fromJson(json.decode(str));

class SearchedImageModel {
  final List<Result> results;
  final Result result;

  SearchedImageModel({this.results, this.result});

  factory SearchedImageModel.fromJson(Map<String, dynamic> json) {
    return SearchedImageModel(
      results: List<Result>.from(
        json['results'].map(
          (x) => Result.fromJson(x),
        ),
      ),
    );
  }
}

class Result {
  final String id;
  final String title;
  final double width;
  final double height;
  final String description;
  final String altDescription;
  final String urlThumb;
  final String urlRegular;
  final String userImage;
  final String userName;
  final DateTime createDate;

  Result(
      {this.id,
      this.title,
      this.width,
      this.height,
      this.description,
      this.altDescription,
      this.urlThumb,
      this.urlRegular,
      this.userImage,
      this.userName,
      this.createDate});

  factory Result.fromJson(Map<String, dynamic> json) {
    return Result(
        id: json['id'],
        title: json['title'],
        width: double.parse(json['cover_photo']['width']?.toString() ?? '0'),
        height: double.parse(json['cover_photo']['height']?.toString() ?? '0'),
        description: json['cover_photo']['description'],
        altDescription: json['cover_photo']['alt_description'],
        urlRegular: json['cover_photo']['urls']['regular'],
        urlThumb: json['cover_photo']['urls']['thumb'],
        userImage: json['user']['profile_image']['large'],
        userName: json['user']['name'],
        createDate: DateTime.parse(json['cover_photo']['created_at']));
  }

  ImageModel toImageModel() {
    return ImageModel(
      id: id,
      height: height,
      title: title,
      width: width,
      createDate: createDate,
      altDescription: altDescription,
      description: description,
      urlRegular: urlRegular,
      urlThumb: urlThumb,
      userImage: userImage,
      userName: userName,
    );
  }
}
