// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_image_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FavoriteImageModelAdapter extends TypeAdapter<FavoriteImageModel> {
  @override
  final int typeId = 2;

  @override
  FavoriteImageModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FavoriteImageModel(
      id: fields[0] as String,
      title: fields[1] as String,
      width: fields[2] as double,
      height: fields[3] as double,
      description: fields[4] as String,
      altDescription: fields[5] as String,
      urlThumb: fields[6] as String,
      urlRegular: fields[7] as String,
      userImage: fields[8] as String,
      userName: fields[9] as String,
      createDate: fields[10] as DateTime,
    );
  }

  @override
  void write(BinaryWriter writer, FavoriteImageModel obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.title)
      ..writeByte(2)
      ..write(obj.width)
      ..writeByte(3)
      ..write(obj.height)
      ..writeByte(4)
      ..write(obj.description)
      ..writeByte(5)
      ..write(obj.altDescription)
      ..writeByte(6)
      ..write(obj.urlThumb)
      ..writeByte(7)
      ..write(obj.urlRegular)
      ..writeByte(8)
      ..write(obj.userImage)
      ..writeByte(9)
      ..write(obj.userName)
      ..writeByte(10)
      ..write(obj.createDate);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FavoriteImageModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
