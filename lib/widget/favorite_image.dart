import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_bloc.dart';
import 'package:search_image_app/bloc/favorites/favorite_image_event.dart';
import 'package:search_image_app/model/image_model.dart';

class FavoriteImages extends StatefulWidget {
  @override
  _FavoriteImagesState createState() => _FavoriteImagesState();
}

class _FavoriteImagesState extends State<FavoriteImages> {
  final ImageModel imageModel;


  _FavoriteImagesState({this.imageModel});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(_getTitle()),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(imageModel.urlThumb),
      ),
      trailing: Container(decoration: BoxDecoration(border: Border.all(color: Colors.black)),
        width: 100,
        child: Row(
          children: <Widget>[
            IconButton(icon: Icon(Icons.delete), onPressed: () {

                BlocProvider.of<FavoriteImageBloc>(context).add(RemoveFromFavorites(imageModel));

            }),
          ],
        ),
      ),
    );
  }
  
  _getTitle(){
    if(imageModel.title==null || imageModel.title==''){
      return Text('No title');
    }else {
      Text(imageModel.title, style: TextStyle(fontWeight: FontWeight.bold));
    }
  }
}
